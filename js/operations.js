var degrees = 0;
var id;
var charTest = [];
function introducirChar(caracter)
{
    charTest.push(caracter);
    console.log(charTest);
}
function calcularNumeroNombre(nombre)
{
    $('#panel').fadeOut('slow');
    console.clear();
    var abc = "abcdefghijklmnopqrstuvwxyz";
    var cont = 0;
    var sum = 0;
    for(let i = 0; i< abc.length; i++)
    {
        cont++;
        if(i == 9 || i == 18)
        {
            cont = 1;
        }
        for(let x = 0; x <= nombre.toString().length; x++)
        {
            if(nombre[x] == abc[i] )
            {
                console.log("el "+nombre[x]+" es el numero "+cont);
                introducirChar({"nombre":nombre[x],"numero":cont});
                sum += cont;
                console.log("suma "+sum);
            }
        }
    }
    setTimeout(function(){
        enviarResultadoPorNombre(sumarCifrasSimplificando(sum))
            .then($('#panel').fadeIn('slow'))
            .catch(function(e){
                alert("Algo falló");
            })
    },540)
}
function enviarResultadoPorNombre(resultado)
{
    var html;
    if(resultado == 1)
    {
        html = "<p>Si tu numeración es el 1 de la suma de tu nombre, es que tienes habilidades y capacidades para ser lider. Tu carácter independiente está ligado a ti. Si haces un uso consciente y adecuado de la energía, acabarás triunfando en todo lo que te propongas</p>";
    }else if(resultado == 2)
    {
        html = "<p>El número 2 da armonía y equilibrio a tus relaciones personales, saber escuchar y tienes empatía natural, puedes adaptarte a cualquier relación que establezcas" +
            "Gran mediados y estableces vínculos sólidos. Paciente y compañero que despirta confianza en las relaciones personales</p>";
    }else if(resultado == 3)
    {
        html = "<p>El número 3 da un carácter afable y divertido, Entusiasta de la vida y curioso por lo que ofrece el día a día. Excelentes artistas y buenos amigos para pasar momentos divertidos. De hacerse querer enseguida y que sabe disfrutar del momento </p>";
    }else if (resultado == 4)
    {
        html = "<p>De carácter analítico y práctico. Le gusta quien va de frente y directamente. ocupado en mejorar su entorno."
        "</p>" +
        "<p> Los demás te verán como una persona demasiado perfeccionista en tu manera de ser y de relacionarse. Aún así eres un amigo fiel que estás dispuesto a aydar a quien realmente lo necesitan</p>";
    }else if (resultado == 5)
    {
        html = "<p>De carácter enérgico y ansioso por vivir al ĺímite te gustan los retos y las pruebas para comprobar tu fortaleza. y no soportas que nadie te reprima" +
            "Eres de mucha actividad física y mental.</p>";
    }else if (resultado == 6)
    {
        html = "<p>Fuente de amor y de sensibilidad tu misión es llevar todo a las personas que lo necesitan. Compasivo y empático siempre protector del débil, y pendiente porque los demás estén bien. La gente confia en ti</p>";
    }else if(resultado == 7)
    {
        html = "<p>Carácter existencialista intentarás descubrir quién realmente eres y qué has venido a hacer al mundo. Te intriga lo misterioso, la concienci, la filosofía" +
            "te gusta nutrirte de una cultura que apague tu fuego de no saber, te gustan los libros y las conversaciones profundas. dedicas tu tiempo a enseñar a los demás valores éticos</p>"
    }else if(resultado == 8)
    {
        html = "<p>Gran poder y fuerza para no tener miedo a los retos que se presentan en la vida. Persona decidida y con los conceptos claros. No tolerás que nadie se interponga entre tí y tus objetivos" +
            " de forma natural llegas a la abundancia y prosperidad a t uvida. Con grandes golpes de suerte que favoreceran la vida en todos los niveles.</p>";

    }else if(resultado == 9)
    {
        html = "<p>Filántropo idealista que hará todo lo que esté en su mano para mejorar su entorno. Intentas defender tus derechos si ves algo que consideras injusto, muy generoso con gran capacidad de sacrificio</p>";
    }else if(resultado == 11 || resultado == 22 || resultado == 23)
    {
        html = "<p>Número mágico. combina el poder del 1 independiente y del 2 sensible</p>";
    }else if(resultado == 22)
    {
        html = "<p>Número mágico. combina el poder del 2 sensible y del 4 constructor</p>";
    }else if(resultado == 33)
    {
        html = "<p>Número mágico. combina el poder del 3 extrovertido y del 6 armoniso</p>";
    }

    $('#puntos').html("Puntuación "+resultado);
    $('#significado').html(html);
    return new Promise(function(){
        var deferred = $.Deferred();
        deferred.resolve(html);
        return deferred.promise();
    });
}
function calcularNumeroPersonalidad(fecha)
{
    $('#panel').fadeOut('slow');
    var fechasinguion = fecha.split("-");
    var fechastring = fechasinguion.toString();
    var res = sumarCifrasSimplificando(fechastring);

    setTimeout(function(){
        $('#panel').fadeIn('slow');
        enviarResultadoPersonalidad(res);
    },500);
}
function calcularNumeroDestino(fecha)
{
    var fechasinguion = fecha.split("-");
    var day = parseInt(fechasinguion[2]);
    var month = parseInt(fechasinguion[1]);
    var year = fechasinguion[0].toString();
    var sumyear = sumarCifrasSinSimplificar(year);
    var total = day + month + sumyear;
    var res = sumarCifrasSimplificando(total.toString());
}
function enviarResultadoPersonalidad(resultado)
{
    var html;
    if(resultado == 1)
    {
        html = "<p>Símbolo de creatividad, independencia, originalidad, liderazgo, ambiciosos, impulsivos y trabajadores. Tu mayor virtud es tu perseverancia, tu peor defecto, tu sentido del individualismo.</p>";
    }else if(resultado == 2)
    {
        html = "<p>Significa que eres empático, muy compañero, sensible, amable, solidario, diplomático pero siempre terminas acaparando toda la atención. Tu mayor virtud es el equilibrio y tu peor defecto es que caes con facilidad en la depresión.</p>";
    }else if(resultado == 3)
    {
        html = "<p>Eres una persona creativa, simpático, con buen sentido del humor, un poco superficial pero sabes comunicar muy bien tus ideas. Tu mayor virtud es que eres muy inteligente y siempre estás generando ideas, tu lado negativo es que no terminas lo que empiezas, porque ni bien empiezas algo, ya estás creando otra idea.</p>";
    }else if (resultado == 4)
    {
        html = "<p>Significa que eres fuerte, práctico, leal, rígido con tus valores inculcados, respetas la justicia y tratas de hacer prevalecer. Tu mayor cualidad des que eres muy responsable y tu mayor defecto es que eres demasiado terco, hasta el punto de convertirte en la persona más seria del mundo.</p>";
    }else if (resultado == 5)
    {
        html = "<p>Eres una persona inquieta, lo tuyo es la libertad, el espíritu viajero, aventurero, instintivo, explorador e ingenioso. Tu mayor cualidad es tu capacidad de hacer amigos y tu peor defecto es que muchas veces la emoción te gana y terminas tomando decisiones apresuradas.</p>";
    }else if (resultado == 6)
    {
        html = "<p>Significa que eres una persona amorosa, responsable, comprensiva, artística, generosa pero muy celosa. Tu mayor virtud es tu trato amable con los demás y tu peor defecto es que eres egocéntrico y muy susceptible a la adulación.</p>";
    }else if(resultado == 7)
    {
        html = "<p>Eres muy espiritual, sabio para tomar decisiones, reflexivo, estudiosos, muy intelectual. Tu mayor cualidad es la cualidad científica o tu habilidad para aprender. Tu mayor virtud es que eres encantador y sabes meditar. Tu defecto es tu perfeccionismo y que a veces eres demasiado reservado con tus cosas.</p>"
    }else if(resultado == 8)
    {
        html = "<p>Significa que eres una persona emprendedora, con capacidad ejecutiva, sacrificado, experto en manejar el poder y la autoridad. Tu mayor virtud es tu habilidad para lograr lo que te propones, tu peor defecto es que eres sumamente ambicioso.</p>";

    }else if(resultado == 9)
    {
        html = "<p>Si tu número de la suerte es el 9 significa que tienes el genio artístico, sentido humanitario romántico, sentimental, desinteresado, amistoso, generoso y persistente. Tu mayor cualidad es tu capacidad de perdonar y tu mayor defecto es que eres muy despistado.</p>";
    }else if(resultado == 11 || resultado == 22 || resultado == 23)
    {
        html = "<p>Número mágico. combina el poder del 1 independiente y del 2 sensible</p>";
    }else if(resultado == 22)
    {
        html = "<p>Número mágico. combina el poder del 2 sensible y del 4 constructor</p>";
    }else if(resultado == 33)
    {
        html = "<p>Número mágico. combina el poder del 3 extrovertido y del 6 armoniso</p>";
    }
    $('#puntos').html("Puntuación "+resultado);
    $('#significado').html(html);

}
function sumarCifrasSinSimplificar(numero)
{
    var sum = 0;
    for(let i = 0; i<numero.length; i++)
    {
        sum += parseInt(numero[i]);
    }
    return sum;
}
function sumarCifrasSimplificando(numero)
{
    var sum = 0;
    if(!isNaN(numero))
    {
        numero = numero.toString();
    }
    for(let i = 0; i< numero.length; i++){

        if(numero[i] != ',')
        {
            console.log("El numero de funcion simplificando "+numero[i]);
            sum +=  parseInt(numero[i]);
        }
    }
    if(sum.toString().length > 1 )
    {
        if(sum != 11 && sum != 22 && sum != 33)
        {
            return sumarCifrasSimplificando(sum.toString());
        }
    }
    return sum;
}

function radioButtons()
{
    if($('input:radio[name=optionsRadios]:checked').val() == 'nombre')
    {
        $('#birth').val('');
        $('#birth').hide();
        $('#fecha-label').hide();
        $('#campo-nombre').show();
    }else
    {
        $('#nombre').val('');
        $('#birth').show();
        $('#fecha-label').show();
        $('#campo-nombre').hide();
    }
}
$('document').ready(function(){
    /* $('nav').show('slow');
     //Scroll para el navegador
     window.addEventListener('scroll',function(evt){
        console.log(window.scrollY);
        if(window.scrollY > 1)
        {
            $('nav').fadeOut('slow');
        }else if(!isNaN(window.scrollY) || window.scrollY < 1)
        {
            $('nav').fadeIn(1800);
        }
     });*/
    radioButtons();
    $('body').bootstrapMaterialDesign();

    $('#panel').hide();

    var conect = false;
    $('input:radio[type=radio]').click(function(){
        radioButtons();
    });
    $('#boton').click(function(){
        charTest = [];
        if($('#birth').val() != '')
        {
            calcularNumeroPersonalidad($('#birth').val());
        }

        if(($('#nombre').val() != '' ))
        {
            var nombre = removeAccents($('#nombre').val().toLowerCase());
            calcularNumeroNombre(nombre);
        }
    });
});
const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}